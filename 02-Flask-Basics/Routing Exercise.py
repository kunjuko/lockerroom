from flask import Flask

app= Flask(__name__)

@app.route("/")
def index():
    return "<h1>Hello, go to http://127.0.0.1:5000//'your_name' to see your name in puppy latin"

@app.route("/<name>")
def info(name):
    if name[-1] == "y":
        puppy_latin_name = name[:-1]+"iful"
    else:
        puppy_latin_name = name+"y"
    return f"Hello {name}, your name in puppy latin is {puppy_latin_name}"

if __name__ == '__main__':
    app.run(debug=True)
