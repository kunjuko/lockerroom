# Set up your imports and your flask app.
from flask import Flask, render_template, request

app = Flask(__name__)
@app.route('/')
def index():
    # This home page should have the form.
    return render_template("index.html")


# This page will be the page after the form
@app.route('/redirect')
def redirect():
    # Check the user name for the 3 requirements.

    # HINTS:
    # https://stackoverflow.com/questions/22997072/how-to-check-if-lowercase-letters-exist/22997094
    # https://stackoverflow.com/questions/26515422/how-to-check-if-last-character-is-integer-in-raw-input

    # Return the information to the report page html.
    username = request.args.get("username")

    uppercase = any(x.isupper() for x in username)
    lowercase = any(x.islower() for x in username)
    number = username[-1] in "0 1 2 3 4 5 6 7 8 9".split()
    valid = (uppercase and lowercase and number)
    return render_template("redirect.html", uppercase=uppercase, lowercase=lowercase, number = number, valid=valid)

if __name__ == '__main__':
    # Fill this in!
    app.run(debug=True)
